# Weather Application

## About the Application:

This application provides the current weather and next 4 days forcast information for 5 european
cities. On landing page user can view high-level information of the selected city. City list added as below.

1. Zurich
2. Amsterdam
3. London
4. Paris
5. Munich

On more details click user can navigate to city's detail weather information.

The application is built using AngularJs 1.x

## Application Structure
### Views
1. City-details.html - Consists details for particular city�s forecast.
2. Home.html - Consists view layout for landing page of the application.
3. Weather.html - Layout used for directive.
### Controllers
1. Main Controller
2. Home Controller
3. Weather Details controller
### Services
1. City Weather service
### Directive
1. Weather info 
### Data
1. List of cities(json)
 - Forecast for a particular city
### Factory
- Notify

## Steps to run the application:

1. Clone code from the bitbucket repository (url:https://bitbucket.org/viratpatel/weather-application)
2. Open command prompt run 'npm install' to download all dependencies
3. run 'npm start' to run application on localhost:8080 
4. To run test cases run 'npm test' in command prompt