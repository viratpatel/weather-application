var app = angular.module('weather-app')
app.controller('WeatherCtrl', function ($scope, CityFactory, CityService, $location) {
    $scope.img = CityService.getRandomColor();
    $scope.isLoading = false;
    $scope.cityForcastList = [];
    var cityDetails = CityService.getSelectedCity();

    /* If user tries to redirect directly to the city page, disallow and redirect to Home  */
    if (!cityDetails) {
        $location.path('/');
        return;
    }

    $scope.isLoading = true;
    CityService.getCityWeather(CityService.getSelectedCity()).then(
        function (res) {
            $scope.cityCoords = res;
            $scope.isLoading = false;
            setDateDetails();
        },
        function (err) {
            console.log(err);
        }
    );

    CityService.getCityWeatherForcast(CityService.getSelectedCity()).then(
        function (res) {
           $scope.cityForcast = res;
           setForcastDetails();
        },
        function (err) {
            console.log(err);
        }
    );

    function setForcastDetails() {
        for (var i = 1; i < 5; i++) {
            $scope.cityForcastList.push($scope.cityForcast.list[i]);
        }
    }

    function setDateDetails() {
        /* Preparing the date object to show the date on which the weather is forecasted */
        var utcSeconds = $scope.cityCoords.dt;
        var myDate = new Date(0);
        myDate.setUTCSeconds(utcSeconds);
        $scope.finalDate = CityService.prepareDate(myDate, true);

        /* Preparing the data for sunrise */
        var utcSunrise = $scope.cityCoords.sys.sunrise;
        var mySunriseTime = new Date(0);
        mySunriseTime.setUTCSeconds(utcSunrise);
        $scope.sunrise = mySunriseTime.toLocaleTimeString();

        /* Preparing the data for sunset */
        var utcSunset = $scope.cityCoords.sys.sunset;
        var mySunsetTime = new Date(0);
        mySunsetTime.setUTCSeconds(utcSunset);
        $scope.sunset = mySunsetTime.toLocaleTimeString();
    }

    CityFactory.onCityChange(function (city) {
        CityService.getCityWeather(city).then(
            function (res) {
                $scope.cityCoords = res;
            },
            function (err) {
                console.log(err);
            }
        );
    });

    /* Preparing format of the date to be rendered on DOM */
    $scope.convertDate = function(dt) {
        var myDate = new Date(0);
        myDate.setUTCSeconds(dt);
        return CityService.prepareDate(myDate, false)
    };

});
