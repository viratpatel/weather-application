var app = angular.module('weather-app');
app.controller('MainCtrl', function ($scope, CityService, $http, $location, $rootScope) {
    $scope.cityCoords;
    $rootScope.selectedCity = '';

    /* Set the values of the cities from the json and storing it into service */
    
    CityService.loadCityInfo().then(    
        function(res) {
            $scope.cityList = res;
            $rootScope.selectedCity = $scope.cityList[0].id.toString();
            CityService.setSelectedCity(getCityObjFromId($scope.cityList[0].id));
        },
        function(err) {
            console.log(err);
        }
    );

    /* Changing the selected city in service on city change event */
    $scope.cityChanged = function (cityId) {
        $scope.selectedCityId = parseInt(cityId);
        CityService.setSelectedCity(getCityObjFromId($scope.selectedCityId));
        $rootScope.selectedCity = cityId;
    }

    /* Function to hide the dropdown when the page is not Home page */
    $scope.$on('$locationChangeStart', function (event) {
        $scope.currentPage = $location.path();
    });

    /* Prepare object from the city Id */
    getCityObjFromId = function (id) {
        var valueToReturn;
        angular.forEach($scope.cityList, function (value, key) {
            if (value.id === id) {
                valueToReturn = value;
            }
        });
        return valueToReturn;
    };

});
