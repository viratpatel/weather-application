var app = angular.module('weather-app')
app.controller('HomeCtrl', function ($scope, CityFactory, CityService, $rootScope) {
    $scope.img = CityService.getRandomColor();
    $scope.isLoading = false;
    $rootScope.selectedCityObj = CityService.getSelectedCity();

    CityService.getCityWeather($rootScope.selectedCityObj).then(    
        function(res) {
            $scope.cityCoords = res; 
        },
        function(err) {
            console.log(err);
        }
    );

    CityFactory.onCityChange(function (city) {
        $scope.isLoading = true;
        CityService.getCityWeather(city).then(    
            function(res) {
                $scope.cityCoords = res; 
                $scope.isLoading = false;
            },
            function(err) {
                console.log(err);
                $scope.isLoading = false;
            }
        );
		$rootScope.selectedCityObj = city;
    });
});
