'use strict';

describe('home-page-ctrl', function() {


  var scope, $scope, CityService, HomeController, flag, $q;
  flag = 'success';

  var weatherInfo = {
    name: 'Zurich',
    id: 2657896,
    dt: 1535561400,
    sys: {type: 1, id: 6016, message: 0.3159, country: "CH", sunrise: 1535517681, sunset: 1535566248}
  };

  var $controller, $rootScope;

  beforeEach(module('weather-app'));

  beforeEach(inject(function(_$controller_, _$rootScope_,_CityService_,_$q_){
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    scope = $rootScope.$new();
    $q = _$q_;
    CityService = _CityService_;
    
    spyOn(CityService, 'getCityWeather').and.callFake(function() {
      return flag === 'success' ? $q.when(weatherInfo) : $q.reject("Error");
    });

    HomeController = $controller('HomeCtrl', {
      $scope: scope,
      CityService: _CityService_
    });

  }));

  describe('The getCityWeather function should exist', function() {
    it('should work', function() {
      expect(HomeController).toBeDefined();
    });
    it('should call getCityWeather', function() {     
      expect(CityService.getCityWeather).toHaveBeenCalled();
    }); 
  });

});