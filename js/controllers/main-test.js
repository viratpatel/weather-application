'use strict';

describe('home-page-ctrl', function() {


  var scope, $scope, CityService, MainController, flag, $q;
  flag = 'success';

  var cityInfo = {
    id: 2657896,
    name: "Zurich",
    country: "CH",
    coord: {
      lon: 47.37,
      lat: 8.55
    }
  };

  var $controller, $rootScope;

  beforeEach(module('weather-app'));

  beforeEach(inject(function(_$controller_, _$rootScope_,_CityService_,_$q_){
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    scope = $rootScope.$new();
    $q = _$q_;
    CityService = _CityService_;
    
    spyOn(CityService, 'loadCityInfo').and.callFake(function() {
      return flag === 'success' ? $q.when(cityInfo) : $q.reject("Error");
    });

    MainController = $controller('MainCtrl', {
      $scope: scope,
      CityService: _CityService_
    });

  }));

  describe('The loadCityInfo function should exist', function() {
    it('should work', function() {
      expect(MainController).toBeDefined();
    });
    it('should call loadCityInfo', function() {     
      expect(CityService.loadCityInfo).toHaveBeenCalled();
    }); 
  });

});