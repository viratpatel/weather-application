'use strict';

describe('weather-detail-ctrl', function() {


  var scope, $scope, CityService, WeatherCtrl, flag, $q;
  flag = 'success';

  var weatherInfo = {
    name: 'Zurich',
    id: 2657896,
    dt: 1535561400,
    sys: {type: 1, id: 6016, message: 0.3159, country: "CH", sunrise: 1535517681, sunset: 1535566248}
  };

  var $controller, $rootScope;

  beforeEach(module('weather-app'));

  beforeEach(inject(function(_$controller_, _$rootScope_,_CityService_,_$q_){
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    scope = $rootScope.$new();
    $q = _$q_;
    CityService = _CityService_;
    
    spyOn(CityService, 'getCityWeather').and.callFake(function() {
      return flag === 'success' ? $q.when(weatherInfo) : $q.reject("Error");
    });

    spyOn(CityService, 'getCityWeatherForcast').and.callFake(function() {
      return flag === 'success' ? $q.when(weatherInfo) : $q.reject("Error");
    });
    
    WeatherCtrl = $controller('WeatherCtrl', {
      $scope: scope,
      CityService: _CityService_
    });

  }));

  describe('The getCityWeather/getCityWeatherForcast function should exist', function() {
    it('should work', function() {
      expect(WeatherCtrl).toBeDefined();
    });
    // it('should call getCityWeather', function() {     
    //   expect(CityService.getCityWeather).toHaveBeenCalled();
    // }); 
    // it('should call getCityWeatherForcast', function() {     
    //   expect(CityService.getCityWeatherForcast).toHaveBeenCalled();
    // });
  });

  describe('function test', function () {
    it('convertDate should convert seconds to long date', function () {
      var $scope = {};
      var controller = $controller('WeatherCtrl', { $scope: $scope });
      
      var myDate = new Date(0);
      myDate.setUTCSeconds(1535561400);
      $scope.finalDate = CityService.prepareDate(myDate, true);
      expect($scope.finalDate).toBe('Wednesday, 29 Aug, 2018');
    });
  });
});