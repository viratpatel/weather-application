'use strict';
describe('ciry-service-test', function(){
    describe('when service call', function(){
        it('returns proper value', function(){
            var $injector = angular.injector([ 'weather-app' ]);
            var myService = $injector.get( 'CityService' );
            expect( myService.randomColor.length ).toEqual(7);
            var myDate = new Date(0);
		    myDate.setUTCSeconds(1535561400);
            expect( myService.prepareDate(myDate, true)).toBe('Wednesday, 29 Aug, 2018');
        })

    })

});