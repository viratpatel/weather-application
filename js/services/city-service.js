var app = angular.module('weather-app')
app.service('CityService', function (CityFactory, $http, $q) {
  this.selectedCity;
  this.cityList;
  this.cityCoords;
  this.baseURI = 'https://api.openweathermap.org/data/2.5';
  this.baseURI1 = 'https://openweathermap.org/data/2.5';
  this.colorList = ["#2196f3","#4caf50","#ff9800","#795548","#92cce2"];
  this.randomColor = this.colorList[Math.floor(Math.random()*5)];

  /* gets the city that is selected in the dropdown */
  this.getSelectedCity = function () {
    return this.selectedCity;
  },

    /* 
      ** sets the city that is selected in the dropdown 
      ** Params - City Object
    */
    this.getRandomColor = function (cityObj) {
      return this.randomColor;
    }
    this.setSelectedCity = function (cityObj) {
      this.selectedCity = cityObj;
      CityFactory.newCity(cityObj);
    },

    /* 
      ** Gets the weather of the selected city
      ** Params - City Object
    */
    this.loadCityInfo = function () {
      var deferred = $q.defer();
      $http({
        method: 'GET',
        url: './data/city.json'
      })
        .success(function (data) {
          deferred.resolve(data);
        }).error(function (data, status, headers, config) {
          deferred.reject("Error: request returned status " + status);
        });
      return deferred.promise;
    },

    this.getCityWeather = function (cityObj) {
      var deferred = $q.defer();
      $http({
        method: 'GET',
        url: this.baseURI + '/weather?id=' + cityObj.id + '&APPID=c6661a63b9e7cca0d4d2f9ffbcb6c967&units=imperial'
      })
        .success(function (data) {
          deferred.resolve(data);
        }).error(function (data, status, headers, config) {
          deferred.reject("Error: request returned status " + status);
        });
      this.randomColor = this.colorList[Math.floor(Math.random()*5)];
      return deferred.promise;
    },

    this.getCityWeatherForcast = function (cityObj) {
      var deferred = $q.defer();
      $http({
        method: 'GET',
        url: this.baseURI1 + '/forecast/daily/?id=' + cityObj.id + '&appid=b6907d289e10d714a6e88b30761fae22&units=imperial'
      })
        .success(function (data) {
          deferred.resolve(data);
        }).error(function (data, status, headers, config) {
          deferred.reject("Error: request returned status " + status);
        });
      return deferred.promise;
    },

    this.prepareDate = function(myDate, isFull) {
        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var dayOfWeek = days[myDate.getDay()];

        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var monthOfYear = months[myDate.getMonth()];

        var year = myDate.getFullYear();
        var dateOfMonth = myDate.getDate();

        if(isFull)
            return dayOfWeek + ', ' + dateOfMonth + ' ' + monthOfYear + ', ' + year;
        else
            return dayOfWeek + ', ' + dateOfMonth + ' ' + monthOfYear;
    };
});