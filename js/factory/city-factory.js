var app = angular.module('weather-app');
app.factory('CityFactory', function () {
    var onCityChangeCallbacks = [];

    var onCityChange = function (callback) {
        onCityChangeCallbacks.push(callback);
    };

    var newCity = function (city) {
        for (var callback in onCityChangeCallbacks) {
            onCityChangeCallbacks[callback](city);
        }
    };

    return {
        onCityChange: onCityChange,
        newCity: newCity
    };
});