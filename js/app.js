var app = angular.module('weather-app', ['ngRoute']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/home', {
            templateUrl: 'views/home.html',
            controller: 'HomeCtrl'
        })
        .when('/city-details', {
            templateUrl: 'views/city-details.html',
            controller: 'WeatherCtrl'
        })
        .otherwise({
            redirectTo: '/home'
        });
});