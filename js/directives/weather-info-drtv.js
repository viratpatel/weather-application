var app = angular.module('weather-app');
    app.directive("weather", function (CityService) {
        return {
            restrict: 'E',
            scope: {
                data: "=",
            },
            templateUrl: 'views/weather.html',
            link: function (scope) {
                var utcSeconds = scope.data.dt;
                var myDate = new Date(0);
                myDate.setUTCSeconds(utcSeconds);

                scope.finalDate = CityService.prepareDate(myDate,true);
            }
        };
    });