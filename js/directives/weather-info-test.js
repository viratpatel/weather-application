describe('counterDirective', function() {
  var compile, scope, directiveElem, element;

  beforeEach(module('weather-app'));

  beforeEach(function(){
    
    inject(function($compile, $rootScope, $templateCache){
      compile = $compile;
      scope = $rootScope.$new();
      $templateCache.put('views/weather.html', directiveElem);
    });
    
  });

  describe('testing directive', function () {
    beforeEach(function () {
      //element = angular.element('<weather></weather>');
      element = compile(directiveElem)(scope);
      scope.finalDate = "Date";
      scope.$digest();
    })
    it('directive contains date or not', function() {
      var spanElement = element.find('span')
      expect(spanElement).toBeDefined();
    });
  });


});